
# Streaming with video or stream source ( Restreaming ) :
#### Facebook Streaming
#### Youtube Streaming
#### Linkedin Streaming
#### Twitch Streaming
#### Vimeo Streaming

## What is Streaming ?
Streaming refers to any media content – live or recorded – delivered to computers and mobile devices via the internet and played back in real time. Podcasts, webcasts, movies, TV shows and music videos are common forms of streaming content.

## How does streaming work ?
Music, video and other types of media files are prearranged and transmitted in sequential packets of data so they can be streamed instantaneously. And unlike traditional downloads that are stored on your device, media files are automatically deleted once you play them.

All you need to stream is a reliable and fast high speed internet connection, access or subscription to a streaming service or app, and a compatible device. See speed recommendations below. 
## What is FFmpeg Live Streaming?

FFmpeg is a command-line tool for converting audio and video formats, and it can also capture and encode in real-time from hardware and software sources. FFmpeg streaming, as you might now guess, is incorporating FFmpeg into your live streaming workflow. It’s a whole suite of tools with significant capabilities for live streaming, it can work with video, audio, and images in almost any format used over the past 20 years and is a free and open-source project meaning it can be downloaded and used at no cost.

## What is Bitrate ?

The number of bits per second that can be transmitted via a digital network is known as a bitrate. The speed of this data is measured in megabits per second (Mbps). Megabits are composed of bits. Eight bits make up one byte. One megabit is equal to 1,024 kilobits which means that 1.0 Mbps is more than 1,000 times faster than 1.0 kilobits per second

Bandwidth is your network’s capacity for uploading and downloading data. It is measured in bits per second (Kbps, Mbps). So, bitrate is a unit of measurement that comes under bandwidth.When it comes to streaming video, get ready for a lot of bandwidth consumption. How much bandwidth, of course, depends on a lot of parameters like content type, file size, video resolution and frame rate, and encoding. When you watch a one hour video, it takes around 500 MB of bandwidth in SD and 2 GB in HD mode.

## What is FPS ?
Frames per second (FPS) is a unit that measures display device performance in video captures and playback and video games. FPS is used to measure frame rate the number of images consecutively displayed each second -- and is a common metric used in video capture and playback when discussing video quality.

The human brain can only process about 10 to 12 FPS. Frame rates faster than this are perceived to be in motion. The greater the FPS, the smoother the video motion appears. Full-motion video is usually 24 FPS or greater




 ### Refer
 
|Video resolution| Video Frame Rate  |Audio Bitrate| Avg Video Bitrate | Avg Size for 1GB Video File| 
|---|---|---|---|---|
|4K [3840x2160]|30 fps  | 128 kb/s | *8500 kb/s | 700MB |  
|QHD [2560x1440]|30 fps  | 128 kb/s | 3854 kb/s | 500MB |  
|FHD-1080p [1920x1080]|30 fps  | 128 kb/s | 2236 kb/s | 400MB |  
|HD-720p [1280x720]|30 fps  | 128 kb/s | 1000 kb/s | 300MB |  
|SD-480p [554x480]|30 fps  | 128 kb/s | 543 kb/s | 210MB |  
|360p [640x360]|30 fps  | 128 kb/s | 400 kb/s | 170MB |
|240p [426x240]|30 fps  | 128 kb/s | 329 kb/s | 140MB |
|144p [144x144]|30 fps  | 128 kb/s | 270 kb/s | 100MB |

##  Installating ffmpeg in ubuntu 

```
 sudo apt install ffmpeg
 ```

### To start the streaming 
Using this you can stream to facebook, youtube, vimeo, linkedin, twitch soon....
Please enter the required details
 ```
 sh start_stream.sh 
 ```
or
 ```
 ./start_stream.sh 
 ```

# ref

[ffmpeg documentation](https://ffmpeg.org/ffmpeg.html)
