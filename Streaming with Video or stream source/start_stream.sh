#! /bin/bash
#Streaming using Shell.
 # Bitrate  of the stream
VarriableBitRate="2500k"           
# FPS  of the stream                        
FPS="30" 
#Quality of the Stream in terms of resolution
Resolution="1920x1080"                
# Source of the stream its may be video file or stream source
#Example1 streamsource = "rtmp://example.com/live/1234"
#Example2 streamsource = "https://example.com/live/1234.m3u8"
#Example3 streamsource = "/home/ubuntu/example.mp4"
StreamSource="example.mp4"
#Streaming URLs                  
StreamingURL="< enter Streaming URL >" 
#Enter Your Streaming Key
StreamingKey="< enter Streaming Key >" 

ffmpeg \
    -stream_loop -1 -i "$StreamSource" -deinterlace \
    -vcodec libx264 -pix_fmt yuv420p -s $Resolution -r $FPS -g $(($FPS * 2)) -b:v $VarriableBitRate \
    -acodec libmp3lame -ar 44100 -threads 6 -qscale 3 -b:a 712000 -bufsize 512k \
    -f flv "$StreamingURL/$StreamingKey"
